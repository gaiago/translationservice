package com.gaiago.translations

import org.assertj.core.api.KotlinAssertions.assertThat
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import java.util.*

@Disabled
class LingoHubTranslationRetrieverTest {
    private val retriever = LingoHubTranslationRetriever(
        organization = "CHANGEME",
        project = "CHANGEME",
        token = "CHANGEME",
        defaultLanguageCode = "en"
    )

    @Test
    fun `retrieve all languages`() {
        val languages = retriever.languages()

        assertThat(languages.first()).isEqualTo("en")
    }

    @Test
    fun `retrieve empty list for any problem`() {
        val languages = LingoHubTranslationRetriever("wrong", "wrong", "wrong-token", "en").languages()

        assertThat(languages).isEmpty()
    }

    @Test
    fun `retrieve translations for one given language`() {
        val translations = retriever.translations("en")

        assertThat(translations["message_greetings_title"]).isEqualTo("Titolo!")
    }

    @Test
    fun `do not retrieve non approved translations source`() {
        val translations = retriever.translations("en")

        assertThat(translations["non_approved_source"]).isEqualTo(null)
    }

    @Test
    fun `do not retrieve non approved translations target`() {
        val translations = retriever.translations("en")

        assertThat(translations["non_approved_target"]).isEqualTo(null)
    }

    @Test
    fun `update missing translation`() {
        val random = Random().nextInt()

        retriever.uploadMissing(mapOf("new_key$random" to "Value $random"))

        Thread.sleep(1000)

        val translations = retriever.translations("en")

        assertThat(translations["new_key$random"]).isEqualTo("Value $random")
    }

    @Test
    fun `do not update already present translation and maintain not present keys`() {
        val random = Random().nextInt()

        retriever.uploadMissing(mapOf("new_key$random" to "Value $random"))

        retriever.uploadMissing(mapOf("new_key$random" to "Value Different"))

        Thread.sleep(1000)

        val translations = retriever.translations("en")

        assertThat(translations["message_greetings_title"]).isEqualTo("Titolo!")
        assertThat(translations["new_key$random"]).isEqualTo("Value $random")
    }

    @Test
    fun `update already present translation`() {
        val random = "1234567890"

        retriever.uploadMissing(mapOf("new_key$random" to "Value $random"))

        retriever.updateExisting(mapOf("new_key$random" to "Value Different"))

        Thread.sleep(1000)

        val translations = retriever.translations("en")

        assertThat(translations["new_key$random"]).isEqualTo("Value Different")
    }
}