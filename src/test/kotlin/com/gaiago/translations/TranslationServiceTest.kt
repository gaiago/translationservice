package com.gaiago.translations

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.KotlinAssertions.assertThat
import org.junit.jupiter.api.Test

class TranslationServiceTest {
    private val retriever: TranslationRetriever = mockk()

    @Test
    fun `translate key to en-GB`() {
        every { retriever.languages() } returns listOf("en-GB")
        every { retriever.translations("en-GB") } returns mapOf("key" to "translated")

        val translated = FallbackTranslationService(retriever).translate(TestLocalizations.key, "en-GB")

        assertThat(translated).isEqualTo("translated")
    }

    @Test
    fun `translate key to en when en-GB is not present`() {
        every { retriever.languages() } returns listOf("en")
        every { retriever.translations("en") } returns mapOf("key" to "translated")

        val translated = FallbackTranslationService(retriever).translate(TestLocalizations.key, "en-GB")

        assertThat(translated).isEqualTo("translated")
    }

    @Test
    fun `translate key to default language when en-GB and en are not present`() {
        every { retriever.defaultLanguageCode() } returns "it"
        every { retriever.languages() } returns listOf("it")
        every { retriever.translations("it") } returns mapOf("key" to "translated")

        val translated = FallbackTranslationService(retriever).translate(TestLocalizations.key, "en-GB")

        assertThat(translated).isEqualTo("translated")
    }

    @Test
    fun `use default value when en-GB, en and the default language are not present`() {
        every { retriever.defaultLanguageCode() } returns "it"
        every { retriever.languages() } returns emptyList()
        every { retriever.uploadMissing(any()) } returns Unit

        val translated = FallbackTranslationService(retriever).translate(TestLocalizations.key, "en-GB")

        verify(exactly = 0) { retriever.translations(any()) }
        assertThat(translated).isEqualTo(TestLocalizations.key.default)
    }

    @Test
    fun `translate key to en when the key in en-GB is not present`() {
        every { retriever.languages() } returns listOf("en", "en-GB")
        every { retriever.translations("en-GB") } returns emptyMap()
        every { retriever.translations("en") } returns mapOf("key" to "translated")

        val translated = FallbackTranslationService(retriever).translate(TestLocalizations.key, "en-GB")

        assertThat(translated).isEqualTo("translated")
    }

    @Test
    fun `translate key to default language when the key in en-GB and in en is not present`() {
        every { retriever.defaultLanguageCode() } returns "it"
        every { retriever.languages() } returns listOf("it", "en", "en-GB")
        every { retriever.translations("en-GB") } returns emptyMap()
        every { retriever.translations("en") } returns emptyMap()
        every { retriever.translations("it") } returns mapOf("key" to "translated")

        val translated = FallbackTranslationService(retriever).translate(TestLocalizations.key, "en-GB")

        assertThat(translated).isEqualTo("translated")
    }

    @Test
    fun `translate key to default language when the keys for en-GB and en are empty`() {
        every { retriever.defaultLanguageCode() } returns "it"
        every { retriever.languages() } returns listOf("it", "en", "en-GB")
        every { retriever.translations("en-GB") } returns mapOf("key" to "")
        every { retriever.translations("en") } returns mapOf("key" to "")
        every { retriever.translations("it") } returns mapOf("key" to "translated")

        val translated = FallbackTranslationService(retriever).translate(TestLocalizations.key, "en-GB")

        assertThat(translated).isEqualTo("translated")
    }

    @Test
    fun `upload translations when the key is not present even for the default language`() {
        every { retriever.defaultLanguageCode() } returns "it"
        every { retriever.languages() } returns listOf("it")
        every { retriever.translations("en-GB") } returns emptyMap()
        every { retriever.translations("en") } returns emptyMap()
        every { retriever.translations("it") } returns mapOf("another-key" to "translated")
        every { retriever.uploadMissing(any()) } returns Unit

        FallbackTranslationService(retriever).translate(TestLocalizations.key, "en-GB")

        verify { retriever.uploadMissing(mapOf("key" to "default")) }
    }

}

enum class TestLocalizations(override val default: String): Localizations {
    key("default");
}