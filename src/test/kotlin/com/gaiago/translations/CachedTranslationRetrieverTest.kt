package com.gaiago.translations

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import java.time.LocalDateTime

class CachedTranslationRetrieverTest {
    private val retriever: TranslationRetriever = mockk(relaxed = true)
    private val clock: Clock = mockk()

    @Test
    fun `should cache languages`() {
        every { retriever.languages() } returns listOf("it", "en")

        val cachedRetriever = CachedTranslationRetriever(retriever, 1)

        cachedRetriever.languages()
        cachedRetriever.languages()

        verify(exactly = 1) { retriever.languages() }
    }

    @Test
    fun `should cache translations`() {
        every { retriever.translations(any()) } returns emptyMap()

        val cachedRetriever = CachedTranslationRetriever(retriever, 1)

        cachedRetriever.translations("it")
        cachedRetriever.translations("it")

        verify(exactly = 1) { retriever.translations("it") }
    }

    @Test
    fun `should cache different translations`() {
        every { retriever.translations(any()) } returns emptyMap()

        val cachedRetriever = CachedTranslationRetriever(retriever, 1)

        cachedRetriever.translations("it")
        cachedRetriever.translations("it")
        cachedRetriever.translations("en")
        cachedRetriever.translations("en")

        verify(exactly = 1) { retriever.translations("it") }
        verify(exactly = 1) { retriever.translations("en") }
    }

    @Test
    fun `cache should be cleared every 60 minutes`() {
        every { clock.now() } returns at("12:00")
        every { retriever.translations(any()) } returns emptyMap()

        val cachedRetriever = CachedTranslationRetriever(retriever, 60, clock)

        every { clock.now() } returns at("12:00")
        cachedRetriever.translations("it")
        every { clock.now() } returns at("12:30")
        cachedRetriever.translations("it")

        every { clock.now() } returns at("13:00")
        cachedRetriever.translations("it")
        every { clock.now() } returns at("13:30")
        cachedRetriever.translations("it")

        every { clock.now() } returns at("14:00")
        cachedRetriever.translations("it")

        verify(exactly = 3) { retriever.translations("it") }
    }

    @Test
    fun `already cached instance`() {
        every { clock.now() } returns at("12:00")
        every { retriever.languages() } returns listOf("it", "en")

        CachedTranslationRetriever.alreadyCached(retriever, 60, clock)

        verify(exactly = 1) { retriever.languages() }
        verify(exactly = 1) { retriever.translations("it") }
        verify(exactly = 1) { retriever.translations("en") }
    }

    private fun at(hourMinutes: String) = LocalDateTime.parse("2020-01-01T$hourMinutes:00.000")
}