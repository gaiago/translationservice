package com.gaiago.translations

import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

interface Clock {
    fun now(): LocalDateTime
}

class RealClock: Clock {
    override fun now(): LocalDateTime {
        return LocalDateTime.now()
    }
}

class CachedTranslationRetriever(
        private val retriever: TranslationRetriever,
        private val resetMinutesInterval: Long,
        private val clock: Clock = RealClock()
): TranslationRetriever {
    private val cache: MutableMap<String, Map<String, String>?> = mutableMapOf()
    private var latestReset: LocalDateTime = clock.now()

    override fun defaultLanguageCode(): String {
        checkReset()
        return retriever.defaultLanguageCode()
    }

    override fun uploadMissing(locales: Map<String, String>) {
        checkReset()
        retriever.uploadMissing(locales)
    }

    override fun updateExisting(locales: Map<String, String>) {
        checkReset()
        retriever.updateExisting(locales)
    }

    override fun translations(languageCode: String): Map<String, String> {
        checkReset()
        return cache[languageCode] ?: retriever.translations(languageCode).andCacheIt(languageCode)
    }

    override fun languages(): List<String> {
        checkReset()
        if(cache.isNotEmpty()) {
            return cache.keys.toList()
        }
        return retriever.languages().andCacheIt()
    }

    private fun checkReset() {
        val now = clock.now()
        if(latestReset.until(now, ChronoUnit.MINUTES) >= resetMinutesInterval) {
            cache.clear()
            latestReset = now
        }
    }

    private fun Map<String, String>.andCacheIt(languageCode: String): Map<String, String> {
        return also { cache[languageCode] = it }
    }

    private fun List<String>.andCacheIt(): List<String> {
        return onEach { languageCode -> cache[languageCode] = null }
    }

    companion object {
        fun alreadyCached(retriever: TranslationRetriever, resetMinutesInterval: Long, clock: Clock = RealClock()): CachedTranslationRetriever {
            return CachedTranslationRetriever(retriever, resetMinutesInterval, clock).also {
                it.languages().onEach { language -> it.translations(language) }
            }
        }
    }
}