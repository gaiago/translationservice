package com.gaiago.translations

interface TranslationService {
    fun translate(key: Localizations, languageCode: String): String
}