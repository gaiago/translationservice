package com.gaiago.translations

interface TranslationRetriever {
    fun uploadMissing(locales: Map<String, String>)
    fun updateExisting(locales: Map<String, String>)
    fun translations(languageCode: String = defaultLanguageCode()): Map<String, String>
    fun languages(): List<String>
    fun defaultLanguageCode(): String
}