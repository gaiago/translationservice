package com.gaiago.translations

interface Localizations {
    val name: String
    val default: String
}