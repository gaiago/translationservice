package com.gaiago.translations

object LingoHubTranslationServiceBuilder {
    fun build(config: LingoHubTranslationConfig): TranslationService {
        val retriever = LingoHubTranslationRetriever(
            config.organization,
            config.project,
            config.token,
            config.defaultLanguageCode
        )

        return when {
            isCachedAtStart(config) -> {
                FallbackTranslationService(CachedTranslationRetriever.alreadyCached(retriever, config.resetCacheMinutesInterval))
            }
            isCached(config) -> { FallbackTranslationService(CachedTranslationRetriever(retriever, config.resetCacheMinutesInterval)) }
            else -> FallbackTranslationService(retriever)
        }
    }

    private fun isCachedAtStart(config: LingoHubTranslationConfig) = isCached(config) && config.cacheAllTranslationAtStart
    private fun isCached(config: LingoHubTranslationConfig) = config.resetCacheMinutesInterval > 0L
}