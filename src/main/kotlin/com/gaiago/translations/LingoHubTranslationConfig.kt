package com.gaiago.translations

data class LingoHubTranslationConfig(
    val organization: String,
    val project: String,
    val token: String,
    val defaultLanguageCode: String,
    val resetCacheMinutesInterval: Long = 0,
    val cacheAllTranslationAtStart: Boolean = false
)