package com.gaiago.translations

class FallbackTranslationService(private val retriever: TranslationRetriever): TranslationService {
    override fun translate(key: Localizations, languageCode: String): String {
        return translateAttempt(key, languageCode)
            ?: translateAttempt(key, languageCode.substring(0..1))
            ?: translateAttempt(key, retriever.defaultLanguageCode())
            ?: localValue(key)
    }

    private fun translateAttempt(key: Localizations, languageCode: String): String? {
        if (retriever.languages().notContains(languageCode)) {
            return null
        }

        return retrieveTranslation(key, languageCode)
    }

    private fun retrieveTranslation(key: Localizations, languageCode: String): String? {
        val translated = retriever.translations(languageCode)[key.name]
        return if (translated.isNullOrEmpty()) null else translated
    }

    private fun localValue(key: Localizations): String {
        updateTranslationKeys(arrayOf(key))
        return key.default
    }

    private fun updateTranslationKeys(locales: Array<Localizations>) {
        retriever.uploadMissing(locales.map { it.name to it.default }.toMap())
    }

    private fun List<String>.notContains(languageCode: String) = !contains(languageCode)
}