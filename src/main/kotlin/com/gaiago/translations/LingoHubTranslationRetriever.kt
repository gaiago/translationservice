package com.gaiago.translations

import com.google.gson.Gson
import com.google.gson.JsonParser.parseString
import topinambur.FieldPart
import topinambur.FilePart
import topinambur.ServerResponse
import topinambur.http

class LingoHubTranslationRetriever(private val organization: String, private val project: String, private val token: String, private val defaultLanguageCode: String): TranslationRetriever {

    override fun defaultLanguageCode() = defaultLanguageCode

    override fun uploadMissing(locales: Map<String, String>) {
        val file = Gson().toJson(locales).toByteArray()

        val response = "${baseUrl()}?auth_token=$token".http.post(
                        data = mapOf(
                                "file" to FilePart(fileFrom(defaultLanguageCode), "application/json", file),
                                "iso2_code" to FieldPart(defaultLanguageCode),
                                "type" to FieldPart("source"),
                                "selectLocalePossible" to FieldPart("true"),
                                "source:createNew" to FieldPart("true"),
                                "source:updateExisting" to FieldPart("false"),
                                "source:deactivateMissing" to FieldPart("false"),
                                "target:createNew" to FieldPart("false"),
                                "target:updateExisting" to FieldPart("false"),
                                "target:deactivateMissing" to FieldPart("false")
                        )
                )

        println(response)
    }

    override fun updateExisting(locales: Map<String, String>) {
        val file = Gson().toJson(locales).toByteArray()

        val response = "${baseUrl()}?auth_token=$token".http.post(
            data = mapOf(
                "file" to FilePart(fileFrom(defaultLanguageCode), "application/json", file),
                "iso2_code" to FieldPart(defaultLanguageCode),
                "type" to FieldPart("source"),
                "selectLocalePossible" to FieldPart("true"),
                "source:createNew" to FieldPart("false"),
                "source:updateExisting" to FieldPart("true"),
                "source:deactivateMissing" to FieldPart("false"),
                "target:createNew" to FieldPart("false"),
                "target:updateExisting" to FieldPart("true"),
                "target:deactivateMissing" to FieldPart("false")
            )
        )

        println(response)
    }

    override fun translations(languageCode: String): Map<String, String> {
        return try {
            val url = baseUrl() + "/" + fileFrom(languageCode)
            val body = parseString(url.getAuthorized().body).asJsonObject

            body.asJsonObject.entrySet().map { it.key to it.value.asString }.toMap()
        } catch(t: Throwable) {
            emptyMap()
        }
    }

    override fun languages(): List<String> {
        return try {
            val body = parseString(baseUrl().getAuthorized().body).asJsonObject
            val members = body.get("members").asJsonArray
            members.map { languageCodeFrom(it.asJsonObject.get("name").asString) }
        } catch(t: Throwable) {
            emptyList()
        }
    }

    private fun languageCodeFrom(fileName: String): String {
        return fileName.split(".")[1]
    }

    private fun fileFrom(languageCode: String) = "backend.$languageCode.json"

    private fun baseUrl() = "https://api.lingohub.com/v1/$organization/projects/$project/resources"

    private fun String.getAuthorized(): ServerResponse {
        return http.get(params = mapOf(
                "auth_token" to token,
                "source:filterByStatus" to "APPROVED",
                "target:filterByStatus" to "APPROVED"
        ))
    }
}